OCI Servers

The purpose of the Cloud Art Admins' OCI Servers repositories are to provide practical servers in a relatively simple and automated fashion.   
As time goes on, these repositories may shift setup processes for starting the servers, however each will eventually recieve documentation at some point.

[TOC]

## Design Choices 
* Each type of server has its own repository.
* These tools/instructions are focused on keeping the information as easy to maintain as possible for as long as possible.
  * Each server will use the images developed by the server's developer.
* This repository will hold all the content for the servers I automate, and provide a method of automatically setting up all the servers one chooses.
* These are functional with Open Cloud Initiative (OCI) standards, so they are intended to work using `podman` and kubernetes, so they will *NOT* necessarily work using `docker`.

## Technical details 
* These tools/instructions are focused on keeping the information as easy to maintain as possible for as long as possible.
  * Each pod will use the "latest" versions of containers, assuming the server can run with the latest containers.
    * \*Nextcloud, currently, needs MariaDB's "10.5" image, for example.
* Each server, unless specified, will be HTTP only.
* By default, each server will have no open ports on its pod.
* The pods are all expected to connect to the HTTPS proxy server.
  * This means admins will be expected to have a domain for accessing the server.
* The servers are expected to all run on one domain, and each server is expected to have its own subdomain.
* Users can go into the individual server's repository to find the server's documentation setup, tools, or further instructions.

## To clone this repository  
Full Clone:
```
git clone --recurse-submodules https://gitlab.com/CloudArtAdmins/oci-servers.git
```

Clone just this repo:
```
git clone https://gitlab.com/CloudArtAdmins/oci-servers.git
```

## To update the submodules (the respositories within this repository)
To update them to this respository's commit:   
```
git submodule update --remote --merge
```

To update to the individual repository's most recent commit:  
```
git submodule foreach git pull
```

## Installation Using CoreOS
##### What this section will not go over:
* Prerequisits
  * [Installation of butane to Create Ignition files](https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/)
  * How to setup DNS with Cloudflare.
  * How to setup a firewall to allow for communication with the server.
    * Port 8080 for HTTP
    * Port 8443 for HTTPS
    * Port 8448 for federation functionalities
  * The basics of SSH. Look at [Farrah's December 25th, 2021](), journal entry (not yet posted).
* [Installation Process of CoreOS.](https://docs.fedoraproject.org/en-US/fedora-coreos/getting-started/)

### Generating the CoreOS Ignition File 
First, the environment file will need to be copied and filled with the desired values:
```
# copy
cp .env.template .env
```
Example .env:
```
# Required to access "core" user 
export SSH_PUBLIC_KEY=ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJcbC7BPACWizKoxKeg2WM7Gi7poTjNyqwz6AhCB0mLB

# Required for HTTPS Proxy 
export DOMAIN=cloudartadmins.com
export DNS_CLOUDFLARE_API_TOKEN=

# Required for Nextcloud 
export NEXTCLOUD_SUBDOMAIN=nextcloud
export NEXTCLOUD_MYSQL_PASSWORD=example_password
export NEXTCLOUD_MYSQL_ROOT_PASSWORD=example_password
export NEXTCLOUD_ADMIN_USER=example_admin
export NEXTCLOUD_ADMIN_PASSWORD=example_password
# Nextcloud Optional
export NEXTCLOUD_REDIS_HOST_PASSWORD=
export NEXTCLOUD_SMTP_HOST=
export NEXTCLOUD_SMTP_SECURE=
export NEXTCLOUD_SMTP_PORT=
export NEXTCLOUD_SMTP_AUTHTYPE=
export NEXTCLOUD_SMTP_NAME=
export NEXTCLOUD_SMTP_PASSWORD=
export NEXTCLOUD_MAIL_FROM_ADDRESS=
export NEXTCLOUD_MAIL_DOMAIN=

# Required for Foundry Virtual Table Top 
export FOUNDRY_SUBDOMAIN=foundry

# Do not modify the following without knowing what they do
export NEXTCLOUD_DOMAIN=$NEXTCLOUD_SUBDOMAIN.$DOMAIN
export FOUNDRY_DOMAIN=$FOUNDRY_SUBDOMAIN.$DOMAIN
export LETSENCRYPT_DOMAIN_LIST="--domain $DOMAIN --domain $NEXTCLOUD_DOMAIN --domain $FOUNDRY_DOMAIN"
```

And lastly, run
```
sh generate_ignition_file.sh
```
which will template the yaml based on the .env values and generate a `coreServers.json` file to use as the ignition file for CoreOS.

If you want a quick nginx container to hold the json file for your coreOS server, you could use the `nginxHTTP.sh` one-liner.

## Backups & Restores
### Backups
The easiest way to backup volumes is by using podman's `export` option and writing the output to a file, structured as follows:
```
podman volume export <volume> > dest.tar
```

Export will output to terminal by default, so one option could also be pulling backups through the network, as follows:
```
ssh user@server.tld podman volume export volume > localFile.tar
```

After the archiving, compression software is often used to help conserve space.
This can be done with many tools, although the best compression (and most resource costly) that the cloud art admins have found uses xz:
```
## Adjust the memory limit to your needs if you want to use this
xz -v -9 --extreme -T0 --memlimit-compress=70% archive.tar
```

To do this manually, if you ever need to, admins can manually archive the backups, too.  
Because of how permissioning works with rootless containers, `podman unshare --rootless-cni` will be prepended to each command written here. 
If you don't use podman's unshare, your user may not have permissions to archive certain files, meaning an incomplete and possibly unusable backup.

As a whole, one can just archive the volumes folder with a command like tar:
```
podman unshare --rootless-cni tar -czf archive.tar.gz -C $HOME/.local/share/containers/storage/ volumes/
```

Which could then be extracted to the same location for a restore:
```
podman unshare --rootless-cni tar -xvf archive.tar.gz -C $HOME/.local/share/containers/storage/
```

### Restoration
If compressed, using the above compression, one will want to first remove the compression:  
```
unxz --keep archive
```  

Then the admin may send the file to the server, and use the following command:  
```
podman import volume_to_add_archive_to archive.tar
```

But if there is not enough storage space for the archive, it may also be sent directly to be directly imported with the following:
```
cat localFile.tar | ssh user@server.tld "podman volume import - "
```

If one doesn't want to use podman for this, the process can be done manually by just extracting the tar file in the `~/.local/share/containers/storage/volumes/` folder.

## Updating the containers
### Using CoreOS or Systemd
If you are using CoreOS, the containers should be updating automatically, thanks to `podman-auto-update.timer` and `podman-auto-update.system`.

### Using setup.sh
These containers are updated by stopping them, removing the entire pod/container, and running the container again after pulling the updated container.  
If you are using the setup.sh script, the process may look something like this:
```
podman pod stop <pod_to_update>
podman pod rm <pod_to_update>
sh setup.sh
```

## Troubleshooting
### Data directory is not writable after data restoration
If you restore CoreOS and recieve an error saying "data directory is not writeable", it is likely fixed by this command:  
```
ssh user@remote.tld podman exec --user root nextcloud chown www-data /var/www/html/data
```
For some reason restoration may not properly set permissions of that volume for the nextcloud folder, leaving it only available to root, which is not the default for the nextcloud container.

### User systemd units/pods fail to start at first boot
Errors may look as follows:
```
Error: repository name must have at least one component
Error: name or ID cannot be empty
```
What causes this is currently unknown, although one can usually just create a podman volume such as with  
```
ssh user@remote.tld podman volume create nextcloud-data
```
then reboot the machine.  
It just works after, a lot of the time.
